/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package slurp;

/**
 *
 * @author Victor Gallen
 */
public class Slurp_Controller {

    private BagPackaging bagpackaging;

    public Slurp_Controller() {
        bagpackaging = new BagPackaging(this);
    }

    /*
    The method we use for calulating the minimum amount of boxes needed for packaging
    Pretty simple. We assume all of the bags are in a liquid state,so we just calculate
    using the volume of all bags that were input in the input fields together.
    We just divide the Tot.Volume of bags with the BoxSize.
    We add 1 to the result incase the rest of the division is anything over 0, since
    we have to round up, because if the result is 4.5 we will need 5 boxes. Pretty obvious :)
    
    I know the solution is not optimal, but to start optimizing for all different ways to stack the
    bags in the different box sizes would take forever. Since the amount of combinations is astronomical.
    However the bags should be pretty easy to pack tightly in a box, so the dead space that is not accounted for
    in this code should be pretty nullified by the fact that +1 is almost always added to the result due to 
    the modulo of TotalVolumeofAllbags and actualBoxSize should never (almost never, statistically speaking) 
    be 0, which means we are almost guaranteed to round up.
    This rounding up should in most cases nullify the fact that we are imagining the bags as liquid volume,
    so this solution is in practice, quite accurate.
     */
    public int calculateMinAmountofBoxes() {
        int minAmountofBoxes = (TotalVolumeofAllBags()) / (actualBoxSize());
        if ((TotalVolumeofAllBags() % actualBoxSize()) == 0) {
            return minAmountofBoxes;
        } else {
            return minAmountofBoxes + 1;
        }
    }

    //Just a simple string to integeter reformatter for future use
    public static int toInt(String StringToReFormat) {
        int realInt = Integer.parseInt(StringToReFormat);
        return realInt;
    }

    /*
    Converting the input strings from the text fields to integers so 
    we can calculate with them later on, doing this for all the bag sizes
    down below
     */

    public int MediumBagsInt() {
        String mediumBags = bagpackaging.getMediumBags();
        int mediumBagsInt = toInt(mediumBags);
        return mediumBagsInt;
    }

    public int LargeBagsInt() {
        String largeBags = bagpackaging.getLargeBags();
        int largeBagsInt = toInt(largeBags);
        return largeBagsInt;
    }

    public int SmallBagsInt() {
        String smallBags = bagpackaging.getSmallBags();
        int smallBagsInt = toInt(smallBags);
        return smallBagsInt;
    }
    public int BoxSizeInt(){
        String boxSize = bagpackaging.getBoxSize();
        int boxSizeInt = toInt(boxSize);
        return boxSizeInt;
    }

    /*Calculating the volume of the box by using the input
    for the boxsize field and multiplying it with itself 2 times
    volume(a) = a^3
     */
    public int actualBoxSize() {
        int CubicSide = BoxSizeInt();
        int CubicSize = (CubicSide * CubicSide * CubicSide);
        return CubicSize;
    }

    /*
    Calculating the volume of the small bags, medium bags and large bags.
    Finally we calculate the totalvolume of all the bags entered into the input
    field combined.
     */
    public int VolumeSmallBags() {
        int volume = (16 * 23 * 2);
        int totalVolume = SmallBagsInt() * volume;
        return totalVolume;
    }

    public int VolumeMediumBags() {
        int volume = (22 * 26 * 2);
        int totalVolume = MediumBagsInt() * volume;
        return totalVolume;
    }

    public int VolumeLargeBags() {
        int volume = (14 * 26 * 10);
        int totalVolume = LargeBagsInt() * volume;
        return totalVolume;
    }

    public int TotalVolumeofAllBags() {
        int totalVolumeofAllBags = VolumeSmallBags() + VolumeMediumBags() + VolumeLargeBags();
        return totalVolumeofAllBags;
    }
}
